package com.saurabi.online.offlineuser.model;

import java.util.List;

import com.saurabi.online.offlineuser.model.RestaurantOrderDetailsDTO;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * @author Supriyo M
 *
 */
@Getter
@EqualsAndHashCode
public class RestaurantOrderDetailsDTO {
	private List<RestaurantItemAddItemDTO> restaurantItemAddItemDTO;

	public RestaurantOrderDetailsDTO(List<RestaurantItemAddItemDTO> restaurantItemAddItemDTO) {
		this.restaurantItemAddItemDTO = restaurantItemAddItemDTO;
	}

	public RestaurantOrderDetailsDTO() {
		super();
	}

}