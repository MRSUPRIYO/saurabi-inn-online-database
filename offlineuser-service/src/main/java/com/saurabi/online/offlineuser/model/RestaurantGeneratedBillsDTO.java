package com.saurabi.online.offlineuser.model;

import java.util.ArrayList;
import java.util.List;

import com.saurabi.online.offlineuser.model.RestaurantGeneratedBillsDTO;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Supriyo M
 *
 */
@Getter
@Setter
@Data
public class RestaurantGeneratedBillsDTO {
	private long orderId;

	private String orderStatus;

	private double orderBillAmount;

	private String orderUserName;

	private String orderPaymentMode;

	private double orderDiscPerc;

	private double orderDiscAmount;

	private double orderNetAmount;

	private List<RestaurantOrderedItemDTO> restaurantOrderedItemDTO = new ArrayList<>();
}
