package com.saurabi.online.offlineuser.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.saurabi.online.offlineuser.model.RestaurantGeneratedBillsDTO;
import com.saurabi.online.offlineuser.model.RestaurantOrderDetailsDTO;
import com.saurabi.online.offlineuser.model.RestaurantOrderDetailsFeedbackDTO;
import com.saurabi.online.offlineuser.service.OfflineuserApplicationFeignClientUser;
import com.saurabi.online.offlineuser.service.OfflineuserApplicationService;

/**
 * @author Supriyo M
 *
 */
@RestController
@RequestMapping("/surabi/offlineuser")
public class OfflineuserApplicationController {

	@Autowired
	OfflineuserApplicationService offlineuserApplicationService;

	@Autowired
	@Lazy
	private OfflineuserApplicationFeignClientUser offlineuserApplicationFeignClientUser;
	
	@GetMapping("/")
	public String home() {
		return "<h1>Saurabhi Inn online-Offline User Service</h1>"+
				"</br><h2 style=\"color:Orange;\">All data transfer from client to "+
				"server or server to client are secured by HTTPS</h2>";
		
	}

	@PostMapping("/bookseats")
	public String bookSeatsService(@RequestParam("userName") String userName,
			@RequestParam("numberOfSeats") int numberOfSeats,
			@RequestParam("bookDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate bookDate) {
		return offlineuserApplicationService.bookSeats(userName, numberOfSeats, bookDate);
	}

	@PostMapping("/feedback")
	public String feedBack(@RequestBody RestaurantOrderDetailsFeedbackDTO restaurantOrderDetailsFeedbackDTO) {
		return offlineuserApplicationService.feedBack(restaurantOrderDetailsFeedbackDTO.getOrdfOrderId(),
				restaurantOrderDetailsFeedbackDTO.getOrdfFeedbackDesc());
	}

	@PostMapping("/confirmorder")
	public String confirmOrder(@RequestParam String userName, @RequestParam String paymentMode, String orderCityName) {
		return offlineuserApplicationService.confirmOrder(userName, paymentMode, orderCityName);
	}

	@PostMapping("/addItemstoOrder")
	public String addItems(String userName, @RequestBody RestaurantOrderDetailsDTO restaurantOrderDetailsDTO) {
		return offlineuserApplicationFeignClientUser.addItemstoOrder(userName, restaurantOrderDetailsDTO);
	}

	@GetMapping("/menu")
	public String getAllItems(@RequestParam String userName) {
		return offlineuserApplicationFeignClientUser.getAllItems(userName);
	}

	@GetMapping("/vieworder")
	public List<RestaurantGeneratedBillsDTO> viewOrder(@RequestParam String userName) {
		return offlineuserApplicationFeignClientUser.viewOrder(userName);
	}

}
