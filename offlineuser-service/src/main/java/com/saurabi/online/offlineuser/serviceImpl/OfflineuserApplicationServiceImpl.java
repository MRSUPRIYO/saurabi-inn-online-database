package com.saurabi.online.offlineuser.serviceImpl;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saurabi.online.offlineuser.entity.BookSeats;
import com.saurabi.online.offlineuser.entity.RestaurantOrderDetails;
import com.saurabi.online.offlineuser.entity.RestaurantOrderDetailsFeedback;
import com.saurabi.online.offlineuser.repository.BookSeatsRepository;
import com.saurabi.online.offlineuser.repository.RestaurantOrderDetailsFeedbackRepository;
import com.saurabi.online.offlineuser.repository.RestaurantOrderDetailsRepository;
import com.saurabi.online.offlineuser.service.OfflineuserApplicationService;

/**
 * @author Supriyo M
 *
 */
@Service
public class OfflineuserApplicationServiceImpl implements OfflineuserApplicationService {

	@Autowired
	BookSeatsRepository bookSeatsRepository;

	@Autowired
	RestaurantOrderDetailsRepository restaurantOrderDetailsRepository;

	@Autowired
	RestaurantOrderDetailsFeedbackRepository restaurantOrderDetailsFeedbackRepository;

	@Override
	public String bookSeats(String userName, int numberOfSeats, LocalDate bookDate) {

		long daysBetween = ChronoUnit.DAYS.between(bookDate, LocalDate.now());

		if (bookDate.compareTo(LocalDate.now()) < 0) {
			return "Seats cannot be booked for past days";
		}

		if (Math.abs(daysBetween) < 2) {
			return "Seats should be booked two days in advance";
		}

		BookSeats bookSeats = new BookSeats();
		bookSeats.setBookDate(bookDate);
		bookSeats.setNumberOfSeats(numberOfSeats);
		bookSeats.setUserName(userName);

		bookSeatsRepository.save(bookSeats);
		return "Seat Booked";
	}

	@Override
	public String feedBack(long orderId, String Feedback) {

		RestaurantOrderDetailsFeedback restaurantOrderDetailsFeedback = new RestaurantOrderDetailsFeedback();
		restaurantOrderDetailsFeedback.setOrdfOrderId(orderId);
		restaurantOrderDetailsFeedback.setOrdfFeedbackDesc(Feedback);
		restaurantOrderDetailsFeedbackRepository.save(restaurantOrderDetailsFeedback);
		return "Feedback Added";
	}

	@Override
	public String confirmOrder(String userName, String paymentMode, String orderCityName) {
		RestaurantOrderDetails restaurantOrderDetails;
		if (restaurantOrderDetailsRepository.existsByOrderStatusAndOrderUserName("CREATED", userName)) {
			restaurantOrderDetails = restaurantOrderDetailsRepository.findByOrderStatusAndOrderUserName("CREATED",
					userName);
			restaurantOrderDetails.setOrderCityName(orderCityName);
			restaurantOrderDetails.setOrderStatus("PAID");
			restaurantOrderDetails.setOrderPaymentMode(paymentMode);
			restaurantOrderDetailsRepository.save(restaurantOrderDetails);
			return "Order No : " + restaurantOrderDetails.getOrderId() + " for user " + userName
					+ " Confirmed and Bill Paid";
		} else {
			throw new RuntimeException("No order created for the user : " + userName + " to confirm and generate bill");
		}
	}

}
