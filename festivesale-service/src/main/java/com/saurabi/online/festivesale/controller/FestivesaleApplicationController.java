package com.saurabi.online.festivesale.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.saurabi.online.festivesale.service.FestivesaleApplicationService;

@RestController
@RequestMapping("/surabi/festivesale")
public class FestivesaleApplicationController {

	@Autowired
	FestivesaleApplicationService festivesaleApplicationService;
	
	@GetMapping("/")
	public String home() {
		return "<h1>Saurabhi Inn online-Festive Sale Service</h1>"+
				"</br><h2 style=\"color:Green;\">All data transfer from client to "+
				"server or server to client are secured by HTTPS</h2>";
		
	}

	@PostMapping("/confirmorder")
	public String confirmOrder(@RequestParam String userName, @RequestParam String festiveCode, String orderCityName) {
		return festivesaleApplicationService.confirmOrder(userName, 50, orderCityName);
	}

}
