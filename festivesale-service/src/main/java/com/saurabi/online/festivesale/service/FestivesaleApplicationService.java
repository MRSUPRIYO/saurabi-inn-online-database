package com.saurabi.online.festivesale.service;

/**
 * @author Supriyo M
 *
 */
public interface FestivesaleApplicationService {

	public String confirmOrder(String userName, int discountPerc, String cityName);
}
