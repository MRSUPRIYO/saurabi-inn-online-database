package com.saurabi.online.festivesale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Supriyo M
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
public class FestivesaleApplication {

	public static void main(String[] args) {
		SpringApplication.run(FestivesaleApplication.class, args);
	}

}
