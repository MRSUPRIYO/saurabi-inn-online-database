CREATE DATABASE IF NOT EXISTS SAURABHI_ONLINE;

SHOW DATABASES;

Use SAURABHI_ONLINE;

DROP TABLE IF EXISTS users;

CREATE TABLE users (
  user_id bigint unsigned NOT NULL AUTO_INCREMENT,
  firstname varchar(100) NOT NULL,
  lastname varchar(100) NOT NULL,
  email varchar(250) NOT NULL,
  password varchar(100) NOT NULL,
  enabled boolean  NOT NULL,
  PRIMARY KEY (user_id),
  UNIQUE KEY `email_unique` (`email`)
);

SHOW TABLES;

DESCRIBE SAURABHI_ONLINE.users;

select COLUMN_NAME, CONSTRAINT_NAME, REFERENCED_COLUMN_NAME, REFERENCED_TABLE_NAME,TABLE_SCHEMA
from information_schema.KEY_COLUMN_USAGE
where TABLE_NAME = 'users' and TABLE_SCHEMA='saurabhi_online';

SHOW INDEX FROM SAURABHI_ONLINE.users;

DROP TABLE IF EXISTS authorities;

  create table authorities (
	 id bigint unsigned NOT NULL AUTO_INCREMENT,
     email varchar(100) NOT NULL,
     authority varchar(50) NOT NULL,
     constraint fk_authorities_users foreign key(email) references users(email),
     PRIMARY KEY (`id`),
     UNIQUE KEY `email_authorities_unique` (`email`, `authority`)
);

DESCRIBE SAURABHI_ONLINE.authorities;

select COLUMN_NAME, CONSTRAINT_NAME, REFERENCED_COLUMN_NAME, REFERENCED_TABLE_NAME
from information_schema.KEY_COLUMN_USAGE
where TABLE_NAME = 'authorities' and TABLE_SCHEMA='saurabhi_online';
     
SHOW INDEX FROM SAURABHI_ONLINE.authorities;     

select * from users;
select * from authorities order by id;

INSERT INTO `users` (`firstname`,`lastname`,`email`,`password`,`enabled`)
VALUES ('supriyo','mukherjee','sup.muk@gmail.com',
'$2a$10$XptfskLsT1l/bRTLRiiCgejHqOpgXFreUnNUa35gJdCr2v2QbVFzu',
true);

INSERT INTO authorities (email, authority)
  values ('sup.muk@gmail.com', 'ROLE_USER');
 
INSERT INTO `users` (`firstname`,`lastname`,`email`,`password`,`enabled`)
VALUES ('admin','mukherjee','adm.muk@gmail.com',
'$2a$10$zxvEq8XzYEYtNjbkRsJEbukHeRx3XS6MDXHMu8cNuNsRfZJWwswDy', true);

INSERT INTO authorities (email, authority)
  values ('adm.muk@gmail.com', 'ROLE_ADMIN');

DROP TABLE IF EXISTS items;
CREATE TABLE items (
  id bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `price` DECIMAL(8, 2) NOT NULL,
  active boolean  NOT NULL,
  PRIMARY KEY (id) 
);

INSERT INTO `items` (`name`,`price`,`active`) VALUES ('Hydrabadi Chicken Biryani',280.00,true);
INSERT INTO `items` (`name`,`price`,`active`) VALUES ('Tandoori Paneer Tikka',220.00,true);
INSERT INTO `items` (`name`,`price`,`active`) VALUES ('Shahi Paneer',210.00,true);
INSERT INTO `items` (`name`,`price`,`active`) VALUES ('Veg Hakka Noodles',140.00,true);
INSERT INTO `items` (`name`,`price`,`active`) VALUES ('Matka Biryani With Raita',220.00,true);
INSERT INTO `items` (`name`,`price`,`active`) VALUES ('Tandoori Chicken',360.00,true);
INSERT INTO `items` (`name`,`price`,`active`) VALUES ('Gobi Manchurian',140.00,true);
INSERT INTO `items` (`name`,`price`,`active`) VALUES ('Fish Green Masala',220.00,true);

select * from items;

DROP TABLE IF EXISTS `orders`;

  CREATE TABLE `saurabhi_online`.`orders` (
  `id` BIGINT unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `order_amount` FLOAT NOT NULL DEFAULT 0,
  `payment_mode` varchar(50),
  `disc_perc` FLOAT NOT NULL DEFAULT 0,
  `disc_amount` FLOAT NOT NULL DEFAULT 0,
  `city_name` varchar(50),
  `grand_total` FLOAT NOT NULL DEFAULT 0,
  `created_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_order_user` (`user_id` ASC),
  CONSTRAINT `fk_order_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `saurabhi_online`.`users` (`user_id`)
   );


   
select * from orders;
   
select max(id) from orders where user_id=6;

select * from orders where DATE(created_at)=DATE(SYSDATE());

select month(created_at),sum(grand_total)
     from orders
     where MONTH(SYSDATE())=MONTH(created_at);


   
DROP TABLE IF EXISTS `order_item`; 
   
CREATE TABLE `saurabhi_online`.`order_item` (
  `id` BIGINT unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint unsigned NOT NULL,
  `item_id` bigint unsigned NOT NULL,
  `price` FLOAT NOT NULL DEFAULT 0,
  `quantity` smallint NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `idx_order_item_order` (`order_id` ASC),
  CONSTRAINT `fk_order_item_order`
    FOREIGN KEY (`order_id`)
    REFERENCES `saurabhi_online`.`orders` (`id`)
   );

ALTER TABLE `saurabhi_online`.`order_item` 
ADD INDEX `idx_order_item_item` (`item_id` ASC);
ALTER TABLE `saurabhi_online`.`order_item` 
ADD CONSTRAINT `fk_order_item_item`
  FOREIGN KEY (`item_id`)
  REFERENCES `saurabhi_online`.`items` (`id`);
  
select * from order_item;
   
DROP TABLE IF EXISTS `audit_log`; 

CREATE TABLE `saurabhi_online`.`audit_log` (
  `id` BIGINT unsigned NOT NULL AUTO_INCREMENT,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(2000) NOT NULL,
   PRIMARY KEY (`id`)
   );
   
select * from audit_log;  
    
 DROP TABLE IF EXISTS `book_seats`;  
 
 create table `saurabhi_online`.`book_seats` ( 
      book_id int primary key not null  auto_increment,
	  username varchar(50) not null,
	  number_of_seats int,
	  book_date date,
      constraint fk_authorities_users_bks foreign key(username) references users(email)
);   

DROP TABLE IF EXISTS `restaurant_order_details_feedback`; 

create table `saurabhi_online`.`restaurant_order_details_feedback` ( 
      ordf_feedback_id int primary key not null auto_increment,
      ordf_order_id int not null,      
	  ordf_feedback_desc varchar(250),
      constraint fk_restaurant_order_details_feedback foreign key(ordf_order_id) references orders(id)
);

DROP VIEW IF EXISTS `vw_restaurant_sales_by_cities`;

create view `saurabhi_online`.`vw_restaurant_sales_by_cities`
(
row_no,
order_city_name,
total_sales
 )
AS   
SELECT ROW_NUMBER() over(order by city_name),
city_name,
SUM(grand_total)
FROM orders
group by city_name;   

select * from vw_restaurant_sales_by_cities;

DROP VIEW IF EXISTS `vw_max_sale_in_month`;

create view `saurabhi_online`.`vw_max_sale_in_month`
(
row_no,
month_year,
max_sale
)
AS
SELECT ROW_NUMBER() over(order by grand_total) ROWNUM,
DATE_FORMAT(created_at,'%b-%Y'),
MAX(grand_total)
FROM orders
GROUP BY DATE_FORMAT(created_at,'%b-%Y');

select * from `vw_max_sale_in_month`;

DROP VIEW IF EXISTS `vw_last_year_monthly_sale`;

create view `saurabhi_online`.`vw_last_year_monthly_sale`
(
row_no,
month_year,
monthly_sale
)
AS
SELECT ROW_NUMBER() over(order by created_at) ROWNUM,
DATE_FORMAT(created_at,'%b-%Y'),
SUM(grand_total)
FROM orders
WHERE YEAR(created_at) = YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))
GROUP BY DATE_FORMAT(created_at,'%b-%Y');

select * from `vw_last_year_monthly_sale`;

DROP PROCEDURE IF EXISTS `FetchAllOrders`;

DELIMITER &&  
CREATE PROCEDURE FetchAllOrders(IN userEmail varchar(250))
BEGIN
select id,user_id,order_amount,payment_mode,disc_perc,disc_amount,city_name,grand_total,created_at 
from orders 
where user_id =(select user_id from users where email=userEmail);
END &&

CALL FetchAllOrders ('abishek.gupta@ymail.com') ;

DROP PROCEDURE IF EXISTS `FetchOrderByPrice`;

DELIMITER && 
CREATE PROCEDURE FetchOrderByPrice(IN userEmail varchar(250),IN salesPrice FLOAT)
BEGIN
select id,user_id,order_amount,payment_mode,disc_perc,disc_amount,city_name,grand_total,created_at 
from orders 
where user_id =(select user_id from users where email=userEmail)
and grand_total=salesPrice;
END &&

CALL FetchOrderByPrice ('abishek.gupta@ymail.com',1104) ;
-------------------------------
DROP PROCEDURE IF EXISTS `FetchOrderByDate`;

DELIMITER && 
CREATE PROCEDURE FetchOrderByDate(IN userEmail varchar(250),IN purchaseDate varchar(200))
BEGIN
select id,user_id,order_amount,payment_mode,disc_perc,disc_amount,city_name,grand_total,created_at 
from orders 
where user_id =(select user_id from users where email=userEmail)
and DATE_FORMAT(created_at, '%d/%m/%Y') =  purchaseDate;
END &&

CALL FetchOrderByDate ('abishek.gupta@ymail.com','24/02/2022') ;