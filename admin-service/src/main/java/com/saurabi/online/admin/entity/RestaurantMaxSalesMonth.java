/**
 * 
 */
package com.saurabi.online.admin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

/**
 * @author Supriyo M
 *
 */
@Entity
@Immutable
@Table(name = "vw_max_sale_in_month")
public class RestaurantMaxSalesMonth {
	
	@Id
	@Column(name = "row_no", insertable = false, updatable = false)
	private Long rowNo;

	@Column(name = "month_year", insertable = false, updatable = false)
	private String monthYear;

	@Column(name = "max_sale", insertable = false, updatable = false)
	private double maxSale;

	/**
	 * @return
	 */
	public Long getRowNo() {
		return rowNo;
	}

	/**
	 * @param rowNo
	 */
	public void setRowNo(Long rowNo) {
		this.rowNo = rowNo;
	}

	/**
	 * @return the monthYear
	 */
	public String getMonthYear() {
		return monthYear;
	}

	/**
	 * @param monthYear the monthYear to set
	 */
	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	/**
	 * @return the maxSale
	 */
	public double getMaxSale() {
		return maxSale;
	}

	/**
	 * @param maxSale the maxSale to set
	 */
	public void setMaxSale(double maxSale) {
		this.maxSale = maxSale;
	}	

}
