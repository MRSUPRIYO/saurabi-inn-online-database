/**
 * 
 */
package com.saurabi.online.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.saurabi.online.admin.entity.RestaurantCityWiseSalesSummary;

/**
 * @author Supriyo M
 *
 */
@Repository
public interface RestaurantCityWiseSalesSummaryRepository extends JpaRepository<RestaurantCityWiseSalesSummary, Long>{

}
