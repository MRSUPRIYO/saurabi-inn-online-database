package com.saurabi.online.admin.repository;

import org.springframework.data.repository.CrudRepository;

import com.saurabi.online.admin.entity.Authority;


public interface AuthorityRepository extends CrudRepository<Authority, Long>{

}
