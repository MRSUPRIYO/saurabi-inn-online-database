/**
 * 
 */
package com.saurabi.online.admin.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saurabi.online.admin.entity.RestaurantCityWiseSalesSummary;
import com.saurabi.online.admin.entity.RestaurantLastYearMonthWiseSales;
import com.saurabi.online.admin.entity.RestaurantMaxSalesMonth;
import com.saurabi.online.admin.repository.RestaurantCityWiseSalesSummaryRepository;
import com.saurabi.online.admin.repository.RestaurantLastYearMonthWiseSalesRepository;
import com.saurabi.online.admin.repository.RestaurantMaxSalesMonthRepository;
import com.saurabi.online.admin.service.RestaurantAdminService;

/**
 * @author Supriyo M
 *
 */

@Service
public class RestaurantAdminServiceImpl implements RestaurantAdminService {
	
	@Autowired
	RestaurantCityWiseSalesSummaryRepository restaurantCityWiseSalesSummaryRepository;
	
	@Autowired
	RestaurantMaxSalesMonthRepository restaurantMaxSalesMonthRepository;
	
	@Autowired
	RestaurantLastYearMonthWiseSalesRepository restaurantLastYearMonthWiseSalesRepository;

	@Override
	public List<RestaurantCityWiseSalesSummary> getCityWiseSales() {
		return restaurantCityWiseSalesSummaryRepository.findAll();
	}
	
	@Override
	public List<RestaurantMaxSalesMonth> getMaxSalesMonth() {
		return restaurantMaxSalesMonthRepository.findAll();
	}
	
	@Override
	public List<RestaurantLastYearMonthWiseSales> getLastYearMonthWiseSales() {
		return restaurantLastYearMonthWiseSalesRepository.findAll();
	}
}
