package com.saurabi.online.admin.serviceImpl;

import java.text.MessageFormat;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import com.saurabi.online.admin.entity.User;
import com.saurabi.online.admin.repository.UserRepository;

@Service
public class JpaUserDetailsManager implements UserDetailsManager {

	  @Autowired
	  private UserRepository repository;

	  @Override
		public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

			final Optional<User> optionalUser = repository.findByEmail(email);

			if (optionalUser.isPresent()) {
				return optionalUser.get();
			}
			else {
				throw new UsernameNotFoundException(MessageFormat.format("User with email {0} cannot be found.", email));
			}
		}

	  @Override
	  public void createUser(UserDetails user) {
	    repository.save((User) user);
	  }

	  @Override
	  public void updateUser(UserDetails user) {
	    repository.save((User) user);
	  }

	  @Override
	  public void deleteUser(String email) {
		  
		  final Optional<User> optionalUser = repository.findByEmail(email);

			if (optionalUser.isPresent()) {
				repository.delete(optionalUser.get());				
			}
			else {
				throw new UsernameNotFoundException(MessageFormat.format("User with email {0} cannot be found.", email));
			}    
	  }

	  /**
	   * This method assumes that both oldPassword and the newPassword params
	   * are encoded with configured passwordEncoder
	   *
	   * @param oldPassword the old password of the user
	   * @param newPassword the new password of the user
	   */
	  @Override
	  @Transactional
	  public void changePassword(String oldPassword, String newPassword) {
		  final Optional<User> optionalUser= repository.findByEmail(oldPassword);
	    if (optionalUser.isPresent()) {
	    	User userDetails =optionalUser.get();
	    	userDetails.setPassword(newPassword);
		    repository.save(userDetails);
		}
		else {
			throw new UsernameNotFoundException("Invalid password");
		}     
	    
	  }

	  @Override
	  public boolean userExists(String email) {
	    return repository.findByEmail(email).isPresent();
	  }
	}