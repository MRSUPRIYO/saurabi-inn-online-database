/**
 * 
 */
package com.saurabi.online.admin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

/**
 * @author Supriyo M
 *
 */
@Entity
@Immutable
@Table(name = "vw_last_year_monthly_sale")
public class RestaurantLastYearMonthWiseSales {

	@Id
	@Column(name = "row_no", insertable = false, updatable = false)
	private Long rowNo;

	@Column(name = "month_year", insertable = false, updatable = false)
	private String monthYear;

	@Column(name = "monthly_sale", insertable = false, updatable = false)
	private double monthlySale;

	/**
	 * @return
	 */
	public Long getRowNo() {
		return rowNo;
	}

	/**
	 * @param rowNo
	 */
	public void setRowNo(Long rowNo) {
		this.rowNo = rowNo;
	}

	/**
	 * @return the monthYear
	 */
	public String getMonthYear() {
		return monthYear;
	}

	/**
	 * @param monthYear the monthYear to set
	 */
	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	/**
	 * @return the monthlySale
	 */
	public double getMonthlySale() {
		return monthlySale;
	}

	/**
	 * @param monthlySale the monthlySale to set
	 */
	public void setMonthlySale(double monthlySale) {
		this.monthlySale = monthlySale;
	}



}
