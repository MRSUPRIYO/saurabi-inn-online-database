/**
 * 
 */
package com.saurabi.online.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.saurabi.online.admin.entity.RestaurantMaxSalesMonth;

/**
 * @author Supriyo M
 *
 */

@Repository
public interface RestaurantMaxSalesMonthRepository extends JpaRepository<RestaurantMaxSalesMonth, Long> {

}
