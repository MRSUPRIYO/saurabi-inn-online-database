package com.saurabi.online.admin.service;

import java.util.List;

import com.saurabi.online.admin.entity.RestaurantCityWiseSalesSummary;
import com.saurabi.online.admin.entity.RestaurantLastYearMonthWiseSales;
import com.saurabi.online.admin.entity.RestaurantMaxSalesMonth;


public interface RestaurantAdminService {

	List<RestaurantCityWiseSalesSummary> getCityWiseSales();
	
	List<RestaurantMaxSalesMonth> getMaxSalesMonth();
	
	List<RestaurantLastYearMonthWiseSales> getLastYearMonthWiseSales();

}