package com.saurabi.online.admin.controller;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.saurabi.online.admin.model.dto.UserDTO;
import com.saurabi.online.admin.utility.AppUtility;
import com.saurabi.online.admin.service.RestaurantAdminService;
import com.saurabi.online.admin.service.RestaurantService;
import com.saurabi.online.admin.entity.RestaurantCityWiseSalesSummary;
import com.saurabi.online.admin.entity.RestaurantLastYearMonthWiseSales;
import com.saurabi.online.admin.entity.RestaurantMaxSalesMonth;
import com.saurabi.online.admin.entity.User;
import com.saurabi.online.admin.exception.UserException;
import com.saurabi.online.admin.model.dto.OrderDTO;

/**
 * @author Supriyo M
 *
 */

@RestController
@RequestMapping("/adminControl")
public class RestaurantAdminController {
	
	@Autowired
	com.saurabi.online.admin.service.UserService UserService;
	
	@Autowired
	RestaurantService  restaurantService;
	
	@Autowired
	RestaurantAdminService restaurantAdminService;
	
	/**
	 * @return
	 */
	@GetMapping("/")
	public String home() {
		return "<h1>Saurabhi Inn online-Admin Service</h1>"+
				"</br><h2 style=\"color:Tomato;\">All data transfer from client to "+
				"server or server to client are secured by HTTPS</h2>";
		
	}
	
	@GetMapping("/showUser")
	public ResponseEntity<UserDTO> showUserByEmail(String email) {
		
		final UserDTO existingUser = AppUtility.getUserDTOFromUser(UserService.loadUserByEmail(email));			
		
		if(existingUser != null) {
			existingUser.setPassword("***********");
			return new ResponseEntity<UserDTO>(existingUser, HttpStatus.CREATED);
		}else {
			return new ResponseEntity<UserDTO>(existingUser, HttpStatus.CONFLICT);
		}
	}
	
	@GetMapping("/user/{email}")
	public String checkIfUserExists(@PathVariable("email") String email) {
		boolean flag = checkIfUserExistsByEmail(email);
		if (flag)
			return "\"" + email + "\" exist in Database";
		else
			return "\"" + email + "\" does not exist in Database";
	}

	public boolean checkIfUserExistsByEmail(String email) {
		return UserService.userExists(email);

	}

	@PostMapping("/createUser")
	public ResponseEntity<String> createUser(@Valid @RequestBody UserDTO userDetails) {

		if (!checkIfUserExistsByEmail(userDetails.getEmail())) {
			final User createdUser = UserService.createSingleUser(userDetails);
			if(createdUser != null) {
				return new ResponseEntity<String>("User Created Successfully", HttpStatus.CREATED);
			}else {
				return new ResponseEntity<String>("User Creation Failed!!", HttpStatus.CONFLICT);
			}
		} else {
			throw new UserException(
					userDetails.getEmail().concat(" already exists. Please try with a different user"));
		}
	}

	@PutMapping("/updateUser")
	public ResponseEntity<String> updateUser(@Valid @RequestBody UserDTO userDetails) {
		if (checkIfUserExistsByEmail(userDetails.getEmail())) {
			final User updatedUser = UserService.updateSingleUser(userDetails);
			if(updatedUser != null) {
				return new ResponseEntity<String>("User updated Successfully", HttpStatus.CREATED);
			}else {
				return new ResponseEntity<String>("User update Failed!!", HttpStatus.CONFLICT);
			}
		} else {
			throw new UserException(
					userDetails.getEmail().concat(" does not exists. Please try with a different user"));
		}
	}

	@DeleteMapping("/deleteUser/{byEmail}")
	public ResponseEntity<String> deleteUser(@PathVariable("byEmail") String email) {
		UserService.deleteSingleUser(email);
		if(checkIfUserExistsByEmail(email)) {
			return new ResponseEntity<String>("User delete Failed!!", HttpStatus.CONFLICT);
		}else {
			return new ResponseEntity<String>("User deleted Successfully", HttpStatus.OK);
		}
	}
	
	@GetMapping("/showBillsGeneratedToday")
	public List<OrderDTO> showAllBillsGeneratedToday() {		
		
		return restaurantService.getAllBillsGeneratedToday();
		
	}
	
	@GetMapping("/showTotalSalesOfCurrentMonth")
	public String showTotalSalesOfCurrentMonth() {	
		
		double totalSalesOfCurrentMonth = restaurantService.getTotalSalesOfCurrentMonth();		
		
		return "Total Sales of "+LocalDate.now().getMonth().toString()+" is "+String.valueOf(totalSalesOfCurrentMonth);
		
	}

	@ExceptionHandler(value = UserException.class)
	public ResponseEntity<String> handleUserException(
			UserException userException) {
		return new ResponseEntity<String>(userException.getMessage(), HttpStatus.CONFLICT);
	}
	

	@GetMapping("/cityWiseSalesSummary")
	public List<RestaurantCityWiseSalesSummary> getCityWiseSales() {
		return restaurantAdminService.getCityWiseSales();

	}

	@GetMapping("/maxSalesInMonth")
	public List<RestaurantMaxSalesMonth> getMaxSalesMonth() {
		return restaurantAdminService.getMaxSalesMonth();

	}

	@GetMapping("/lastYearMonthWiseSales")
	public List<RestaurantLastYearMonthWiseSales> getMonthWiseSales() {
		return restaurantAdminService.getLastYearMonthWiseSales();

	}

}
