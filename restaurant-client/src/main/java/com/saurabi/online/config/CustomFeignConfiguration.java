package com.saurabi.online.config;

import java.io.IOException;
import java.nio.file.Paths;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
public class CustomFeignConfiguration {

    @Bean
    public void Config() throws IOException {  
    	ClassPathResource resource = new ClassPathResource("client_ssl.p12");
        System.setProperty("javax.net.ssl.keyStoreType", "PKCS12");    
        System.setProperty("javax.net.ssl.keyStore", Paths.get(resource.getURI()).toString());  
        System.setProperty("javax.net.ssl.keyStorePassword", "client333"); 
    }

}
