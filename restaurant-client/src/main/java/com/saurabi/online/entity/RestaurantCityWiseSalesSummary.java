/**
 * 
 */
package com.saurabi.online.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

/**
 * @author Supriyo M
 *
 */

@Entity
@Immutable
@Table(name = "vw_restaurant_sales_by_cities")
public class RestaurantCityWiseSalesSummary {

	@Id
	@Column(name = "row_no", insertable = false, updatable = false)
	private Long rowNo;

	@Column(name = "order_city_name", insertable = false, updatable = false)
	private String orderCityName;

	@Column(name = "total_sales", insertable = false, updatable = false)
	private double totalSales;



	/**
	 * @return
	 */
	public Long getRowNo() {
		return rowNo;
	}

	/**
	 * @param rowNo
	 */
	public void setRowNo(Long rowNo) {
		this.rowNo = rowNo;
	}

	/**
	 * @return
	 */
	public String getOrderCityName() {
		return orderCityName;
	}

	/**
	 * @param orderCityName
	 */
	public void setOrderCityName(String orderCityName) {
		this.orderCityName = orderCityName;
	}

	/**
	 * @return the totalSales
	 */
	public double getTotalSales() {
		return totalSales;
	}

	/**
	 * @param totalSales the totalSales to set
	 */
	public void setTotalSales(double totalSales) {
		this.totalSales = totalSales;
	}

}
