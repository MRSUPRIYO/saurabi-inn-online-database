package com.saurabi.online.model.dto;

import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserDTO {

	@ApiModelProperty(value = "First name of the user", name = "firstName", dataType = "String", example = "Amisha")
	@NotBlank(message = "First name can not be empty")
	private String firstname;

	@ApiModelProperty(value = "Last name of the user", name = "lastname", dataType = "String", example = "Gupta")
	@NotBlank(message = "Last name can not be empty")
	private String lastname;

	@ApiModelProperty(value = "Email of the user", name = "email", dataType = "String", example = "amisha.gupta@gmail.com")
	@NotBlank(message = "Email can not be empty")
	@Email(message = "Please provide a valid email id")
	private String email;

	@ApiModelProperty(value = "Password of the user", name = "password", dataType = "String", example = "password")
	@NotBlank(message = "Password can not be empty")
	private String password;

	@ApiModelProperty(value = "Roles", name = "authorities", dataType = "Set<String>", example = "[\"USER\",\"ADMIN\"]")
	@NotNull(message = "Authorities can not be empty")
	private Set<String> authorities;

}
