package com.saurabi.online.model.dto;

import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * @author Supriyo M
 *
 */
@Getter
@EqualsAndHashCode
public class OrderItemDetailsDTO {
	private List<CreateOrderItemDTO> restaurantItemAddItemDTO;

	public OrderItemDetailsDTO(List<CreateOrderItemDTO> restaurantItemAddItemDTO) {
		this.restaurantItemAddItemDTO = restaurantItemAddItemDTO;
	}

	public OrderItemDetailsDTO() {
		super();
	}

}