package com.saurabi.online.model.dto;

import java.util.Date;
import lombok.Data;

@Data
public class OrderDTO {

	private Long billNo;
	
	private double grandTotal;
	
	private Long user_id;
	
	private String email;
	
	private Date createdAt;
	
}
