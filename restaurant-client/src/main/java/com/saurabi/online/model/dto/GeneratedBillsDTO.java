package com.saurabi.online.model.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class GeneratedBillsDTO {
	private long orderId;

	private String orderStatus;

	private double orderBillAmount;

	private String orderUserName;

	private String orderPaymentMode;

	private double orderDiscPerc;

	private double orderDiscAmount;

	private double orderNetAmount;

	private List<CreateOrderItemDTO> restaurantOrderedItemDTO = new ArrayList<>();
}
