/**
 * 
 */
package com.saurabi.online.model.dto;

import java.util.Date;

import lombok.Data;

/**
 * @author Supriyo M
 *
 */
@Data
public class OrderDetailsDTO {

	private Long billNo;
	
	private String email;
	
	private double orderAmount;
	
	private double discPerc;
	
	private double discAmount;
	
	private String paymentMode;
	
	private double grandTotal;
	
	private String cityName;
	
	private Date billDate;	

}
