package com.saurabi.online.service;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.time.LocalDate;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Lazy;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.saurabi.online.model.dto.GeneratedBillsDTO;
import com.saurabi.online.model.dto.OrderItemDetailsDTO;
import com.saurabi.online.model.dto.OrderDetailsFeedbackDTO;

/**
 * @author Supriyo M
 *
 */
@FeignClient(name="offlineuser-service")
@Lazy
public interface RestaurantOfflineUserClient {
	@RequestMapping(value = "/surabi/offlineuser/bookseats", method = POST)
	public String bookSeats(@RequestParam("userName") String userName, @RequestParam("numberOfSeats") int numberOfSeats, @RequestParam("bookDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate bookDate) ;
	
	@RequestMapping(value = "/surabi/offlineuser/feedback", method = POST)
	public String feedBack(@RequestBody OrderDetailsFeedbackDTO restaurantOrderDetailsFeedbackDTO) ;
	
	@RequestMapping(value = "/surabi/offlineuser/confirmorder", method = POST)
	public String confirmorder(@RequestParam String userName,@RequestParam String paymentMode,@RequestParam(name = "orderCityName") String orderCityName) ;
	
	@RequestMapping(value = "/surabi/offlineuser/addItemstoOrder", method = POST)
	public String addItemstoOrder(@RequestParam(name = "userName") String userName,@RequestBody OrderItemDetailsDTO restaurantOrderDetailsDTO);
	
	@RequestMapping(value = "/surabi/offlineuser/menu", method = GET)
	public String getAllItems(@RequestParam(name = "userName") String userName);
	
	@RequestMapping(value = "/surabi/offlineuser/vieworder", method = GET)
	public List<GeneratedBillsDTO> viewOrder(@RequestParam(name = "userName") String userName);
}
