/**
 * 
 */
package com.saurabi.online.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.saurabi.online.entity.RestaurantCityWiseSalesSummary;
import com.saurabi.online.entity.RestaurantLastYearMonthWiseSales;
import com.saurabi.online.entity.RestaurantMaxSalesMonth;
import com.saurabi.online.config.CustomFeignConfiguration;
import com.saurabi.online.model.dto.OrderDTO;
import com.saurabi.online.model.dto.UserDTO;

/**
 * @author Supriyo M
 *
 */

@FeignClient(name = "admin-service", url="https://localhost:8222", configuration = CustomFeignConfiguration.class)
public interface RestaurantAdminClient {

	@GetMapping("/adminControl/showUser")
	ResponseEntity<UserDTO> showUserByEmail(@RequestParam("email") String email);
	
	@GetMapping("/adminControl/user/{email}")
	String checkIfUserExists(@PathVariable("email") String email);
	
	@PostMapping("/adminControl/createUser")
	ResponseEntity<String> createUser(@Valid @RequestBody UserDTO userDetails);
	
	@PutMapping("/adminControl/updateUser")
	ResponseEntity<String> updateUser(@Valid @RequestBody UserDTO userDetails);
	
	@DeleteMapping("/adminControl/deleteUser/{byEmail}")
	ResponseEntity<String> deleteUser(@PathVariable("byEmail") String email);
	
	@GetMapping("/adminControl/showBillsGeneratedToday")
	List<OrderDTO> showAllBillsGeneratedToday();
	
	@GetMapping("/adminControl/showTotalSalesOfCurrentMonth")
	String showTotalSalesOfCurrentMonth();	
	
	@GetMapping("/adminControl/cityWiseSalesSummary")
	List<RestaurantCityWiseSalesSummary> getCityWiseSales();
	
	@GetMapping("/adminControl/maxSalesInMonth")
	List<RestaurantMaxSalesMonth> getMaxSalesMonth();
	
	@GetMapping("/adminControl/lastYearMonthWiseSales")
	List<RestaurantLastYearMonthWiseSales> getMonthWiseSales();
	
}
