package com.saurabi.online.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestaurantClientController {
	
	/**
	 * @return
	 */
	@GetMapping("/")
	public String home() {
		return "<h1>Saurabhi Inn online-Client Service</h1>"+
				"</br><h2 style=\"color:Blue;\">All data transfer from client to "+
				"server or server to client are secured by HTTPS</h2>";
		
	}

}
