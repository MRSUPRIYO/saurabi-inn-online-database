----------------------------------------------------------------------------------
DROP VIEW IF EXISTS `vw_restaurant_sales_by_cities`;

create view `saurabhi_online`.`vw_restaurant_sales_by_cities`
(
row_no,
order_city_name,
total_sales
 )
AS   
SELECT ROW_NUMBER() over(order by city_name),
city_name,
SUM(grand_total)
FROM orders
group by city_name;   

select * from vw_restaurant_sales_by_cities;

----------------------------------------------------------------------------------

DROP VIEW IF EXISTS `vw_max_sale_in_month`;

create view `saurabhi_online`.`vw_max_sale_in_month`
(
row_no,
month_year,
max_sale
)
AS
SELECT ROW_NUMBER() over(order by grand_total) ROWNUM,
DATE_FORMAT(created_at,'%b-%Y'),
MAX(grand_total)
FROM orders
GROUP BY DATE_FORMAT(created_at,'%b-%Y');

select * from `vw_max_sale_in_month`;

----------------------------------------------------------------------------------

DROP VIEW IF EXISTS `vw_last_year_monthly_sale`;

create view `saurabhi_online`.`vw_last_year_monthly_sale`
(
row_no,
month_year,
monthly_sale
)
AS
SELECT ROW_NUMBER() over(order by created_at) ROWNUM,
DATE_FORMAT(created_at,'%b-%Y'),
SUM(grand_total)
FROM orders
WHERE YEAR(created_at) = YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))
GROUP BY DATE_FORMAT(created_at,'%b-%Y');

select * from `vw_last_year_monthly_sale`;

----------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `FetchAllOrders`;

DELIMITER &&  
CREATE PROCEDURE FetchAllOrders(IN userEmail varchar(250))
BEGIN
select id,order_amount,payment_mode,disc_perc,disc_amount,city_name,grand_total,created_at 
from orders 
where user_id =(select user_id from users where email=userEmail);
END &&

CALL FetchAllOrders ('abishek.gupta@ymail.com') ;

----------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `FetchOrderByPrice`;

DELIMITER && 
CREATE PROCEDURE FetchOrderByPrice(IN userEmail varchar(250),IN salesPrice FLOAT)
BEGIN
select id,order_amount,payment_mode,disc_perc,disc_amount,city_name,grand_total,created_at 
from orders 
where user_id =(select user_id from users where email=userEmail)
and grand_total=salesPrice;
END &&

CALL FetchOrderByPrice ('abishek.gupta@ymail.com',1104) ;

----------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `FetchOrderByDate`;

DELIMITER && 
CREATE PROCEDURE FetchOrderByDate(IN userEmail varchar(250),IN purchaseDate varchar(200))
BEGIN
select id,order_amount,payment_mode,disc_perc,disc_amount,city_name,grand_total,created_at 
from orders 
where user_id =(select user_id from users where email=userEmail)
and DATE_FORMAT(created_at, '%d/%m/%Y') =  purchaseDate;
END &&

CALL FetchOrderByDate ('abishek.gupta@ymail.com','24/02/2022') ;
----------------------------------------------------------------------------------