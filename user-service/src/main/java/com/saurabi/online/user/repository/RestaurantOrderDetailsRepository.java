/**
 * 
 */
package com.saurabi.online.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.saurabi.online.user.entity.Order;

/**
 * @author Supriyo M
 *
 */
@Repository
public interface RestaurantOrderDetailsRepository extends JpaRepository<Order, Long>{

	@Query(nativeQuery = true, value = "CALL FetchAllOrders(:email)")
	List<Order> fetchAllOrders(String email);
	
	@Query(nativeQuery = true, value = "CALL FetchOrderByPrice(:email, :price)")
	List<Order> fetchByPrice(String email, Double price);
	
	@Query(nativeQuery = true, value = "CALL FetchOrderByDate(:email, :date)")
	List<Order> fetchByPurchaseDate(String email, String date);
}
