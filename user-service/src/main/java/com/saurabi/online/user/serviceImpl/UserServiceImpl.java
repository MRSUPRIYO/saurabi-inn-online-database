package com.saurabi.online.user.serviceImpl;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.saurabi.online.user.entity.Authority;
import com.saurabi.online.user.entity.User;
import com.saurabi.online.user.entity.UserRole;
import com.saurabi.online.user.exception.UserException;
import com.saurabi.online.user.model.dto.UserDTO;
import com.saurabi.online.user.repository.AuthorityRepository;
import com.saurabi.online.user.repository.UserRepository;
import com.saurabi.online.user.service.UserService;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userDetailsRepository;

	@Autowired
	private AuthorityRepository grantedAuthorityRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public User loadUserById(long id) throws UserException {
		
		final Optional<User> optionalUser =userDetailsRepository.findById(id);
		
		if (optionalUser.isPresent()) {
			return optionalUser.get();
		} else {
			throw new UserException(MessageFormat.format("User with id {0} cannot be found.", id));
		}		
	}

	@Override
	public boolean userExists(String email) {
		return userDetailsRepository.findByEmail(email).isPresent();
	}

	@Override
	public User loadUserByEmail(String email) throws UserException {

		final Optional<User> optionalUser = userDetailsRepository.findByEmail(email);

		if (optionalUser.isPresent()) {
			return optionalUser.get();
		} else {
			throw new UserException(MessageFormat.format("User with email {0} cannot be found.", email));
		}
	}

	@Override
	public User createSingleUser(UserDTO userDetails) {

		User user = User.builder().firstname(userDetails.getFirstname()).lastname(userDetails.getLastname())
				.email(userDetails.getEmail()).password(passwordEncoder.encode(userDetails.getPassword())).enabled(true)
				.build();
		User newUser = userDetailsRepository.save(user);

		if (newUser != null) {

			Set<String> grantedAuthorities = userDetails.getAuthorities();
			Set<Authority> authorities = new HashSet<>();

			for (String grantedAuthority : grantedAuthorities) {
				Authority authority = new Authority();
				authority.setAuthority("ROLE_".concat(grantedAuthority.replace("ROLE_", "")));
				authority.setUserDetails(newUser);
				grantedAuthorityRepository.save(authority);
				authorities.add(authority);
			}

			if (grantedAuthorities == null || grantedAuthorities.size() == 0) {
				Authority authority = new Authority();
				authority.setAuthority("ROLE_".concat(UserRole.USER.name()));
				grantedAuthorityRepository.save(authority);
				newUser.setAuthorities(Collections.singleton(authority));
			} else {
				newUser.setAuthorities(authorities);
			}

		} else {
			throw new UserException(
					MessageFormat.format("User with email {0} creation failed", userDetails.getEmail()));
		}

		return newUser;

	}

	@Override
	public User updateSingleUser(UserDTO userDetails) {

		User existingUser = loadUserByEmail(userDetails.getEmail());
		existingUser.setFirstname(userDetails.getFirstname());
		existingUser.setLastname(userDetails.getLastname());
		existingUser.setPassword(passwordEncoder.encode(userDetails.getPassword()));
		existingUser.getAuthorities().clear();

		User updatedUser = userDetailsRepository.save(existingUser);

		if (updatedUser != null) {

			Set<String> grantedAuthorities = userDetails.getAuthorities();
			Set<Authority> authorities = new HashSet<>();

			for (String grantedAuthority : grantedAuthorities) {
				Authority authority = new Authority();
				authority.setAuthority("ROLE_".concat(grantedAuthority.replace("ROLE_", "")));
				authority.setUserDetails(updatedUser);
				grantedAuthorityRepository.save(authority);
				authorities.add(authority);
			}

			if (grantedAuthorities == null || grantedAuthorities.size() == 0) {
				Authority authority = new Authority();
				authority.setAuthority("ROLE_".concat(UserRole.USER.name()));
				grantedAuthorityRepository.save(authority);
				updatedUser.setAuthorities(Collections.singleton(authority));
			} else {
				updatedUser.setAuthorities(authorities);
			}

		} else {
			throw new UserException(MessageFormat.format("User with email {0} update failed", userDetails.getEmail()));
		}

		return updatedUser;

	}

	@Override
	public void deleteSingleUser(String email) throws UserException {
		User existingUser = loadUserByEmail(email);
		if (existingUser != null) {
			userDetailsRepository.delete(existingUser);
		}
	}
}