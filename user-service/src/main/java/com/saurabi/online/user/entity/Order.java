package com.saurabi.online.user.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "Orders")
public class Order {
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;	
	
	@Column(name="ORDER_AMOUNT")
	private double orderAmount;
	
	@Column(name="PAYMENT_MODE")
	private String paymentMode;
	
	@Column(name="DISC_PERC")
	private double discPerc;
	
	@Column(name="DISC_AMOUNT")
	private double discAmount;
	
	@Column(name="CITY_NAME")
	private String cityName;
	
	@Column(name="GRAND_TOTAL")
	private double grandTotal;
	
	@Column(name="CREATED_AT")
	private Date createdAt;
	
	@ManyToOne
	@JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
	private User userDetails;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name="ORDER_ID", referencedColumnName="ID")
	private List<OrderItem> orderItemList;

}
