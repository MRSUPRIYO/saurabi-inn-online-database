package com.saurabi.online.user.utility;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.saurabi.online.user.entity.Item;
import com.saurabi.online.user.entity.Order;
import com.saurabi.online.user.entity.OrderItem;
import com.saurabi.online.user.entity.User;
import com.saurabi.online.user.model.dto.CreateOrderDTO;
import com.saurabi.online.user.model.dto.CreateOrderItemDTO;
import com.saurabi.online.user.model.dto.ItemDTO;
import com.saurabi.online.user.model.dto.OrderDTO;
import com.saurabi.online.user.model.dto.OrderDetailsDTO;
import com.saurabi.online.user.model.dto.UserDTO;
import com.saurabi.online.user.model.dto.UserDetailsDTO;

public class AppUtility {

	public static UserDTO getUserDTOFromUserDetailsDTO(@Valid UserDetailsDTO userDetails) {

		if (userDetails != null) {
			UserDTO userDTO = new UserDTO();
			userDTO.setFirstname(userDetails.getFirstname());
			userDTO.setLastname(userDetails.getLastname());
			userDTO.setPassword(userDetails.getPassword());
			userDTO.setEmail(userDetails.getEmail());

			return userDTO;
		}
		return null;
	}

	public static UserDTO getUserDTOFromUser(User user) {
		if (user != null) {
			UserDTO userDTO = new UserDTO();
			userDTO.setFirstname(user.getFirstname());
			userDTO.setLastname(user.getLastname());
			userDTO.setPassword(user.getPassword());
			userDTO.setEmail(user.getEmail());
			userDTO.setAuthorities(user.getAuthorities().stream().map(e -> e.getAuthority().replace("ROLE_", ""))
					.collect(Collectors.toSet()));
			return userDTO;
		}
		return null;
	}
	
	public static ItemDTO getItemDTOFromItem(Item item) {
		
		if (item != null) {
			ItemDTO itemDTO = new ItemDTO();
			itemDTO.setItemId(item.getId());
			itemDTO.setName(item.getName());
			itemDTO.setPrice(item.getPrice());
			return itemDTO;
		}
		
		return null;
		
	}

	public static String getLoggedInUserName() {
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username ="";

		if (principal instanceof UserDetails) {
			username = ((UserDetails)principal).getUsername();
		} else {
			username = principal.toString();
		}
		
		return username;
	}

	public static OrderDTO getOrderDTOFromOrder(Order order) {
		if (order != null) {
			
			OrderDTO orderDTO = new OrderDTO();
			orderDTO.setCreatedAt(order.getCreatedAt());
			orderDTO.setGrandTotal(order.getGrandTotal());
			orderDTO.setBillNo(order.getId());
			orderDTO.setUser_id(order.getUserDetails().getUser_id());
			orderDTO.setEmail(order.getUserDetails().getEmail());
			return orderDTO;
		}
		return null;
	}
	
	public static OrderDetailsDTO getOrderDetailsDTOFromOrder(Order order) {
		if (order != null) {
			
			OrderDetailsDTO orderDetailsDTO = new OrderDetailsDTO();
			orderDetailsDTO.setBillNo(order.getId());
			orderDetailsDTO.setEmail(order.getUserDetails().getEmail());
			orderDetailsDTO.setOrderAmount(order.getOrderAmount());
			orderDetailsDTO.setDiscPerc(order.getDiscPerc());
			orderDetailsDTO.setDiscAmount(order.getDiscAmount());
			orderDetailsDTO.setPaymentMode(order.getPaymentMode());
			orderDetailsDTO.setGrandTotal(order.getGrandTotal());
			orderDetailsDTO.setCityName(order.getCityName());			
			orderDetailsDTO.setBillDate(order.getCreatedAt());
			return orderDetailsDTO;
		}
		return null;
	}
	
	public static List<OrderDTO> getOrderDTOListFromOrderList(List<Order> orders) {
		
		if (orders != null) {
		
			List<OrderDTO> OrderDTOList = new ArrayList<>();
			for (Order Order : orders) {
				
				OrderDTO orderDTO = getOrderDTOFromOrder(Order);
				if (orderDTO != null) {			
					OrderDTOList.add(orderDTO);
				}
			}
			
			return OrderDTOList;
		}
		
		return null;		
	}
	
	public static List<OrderDetailsDTO> getOrderDetailsDTOListFromOrderList(List<Order> orders) {
		
		if (orders != null) {
		
			List<OrderDetailsDTO> OrderDetailsDTOList = new ArrayList<>();
			for (Order Order : orders) {
				
				OrderDetailsDTO orderDetailsDTO = getOrderDetailsDTOFromOrder(Order);
				if (orderDetailsDTO != null) {			
					OrderDetailsDTOList.add(orderDetailsDTO);
				}
			}
			
			return OrderDetailsDTOList;
		}
		
		return null;		
	}
	
	public static CreateOrderDTO getCreateOrderDTOFromOrder(Order order) {
		if (order != null) {
			
			CreateOrderDTO cOrderDTO = new CreateOrderDTO();
			List<CreateOrderItemDTO> orderItemList = new ArrayList<>();
			
			for (OrderItem oItem : order.getOrderItemList()) {
				
				CreateOrderItemDTO coiDTO = new CreateOrderItemDTO();
				coiDTO.setItemId(oItem.getId());
				coiDTO.setQuantity(oItem.getQuantity());
				orderItemList.add(coiDTO);
			}
			cOrderDTO.setOrderItemList(orderItemList);
			return cOrderDTO;
		}
		return null;
	}

}
