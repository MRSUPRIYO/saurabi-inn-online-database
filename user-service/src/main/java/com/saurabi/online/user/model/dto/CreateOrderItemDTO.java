package com.saurabi.online.user.model.dto;

import lombok.Data;

@Data
public class CreateOrderItemDTO {

	
	private Long itemId;


	private int quantity;
	
}
